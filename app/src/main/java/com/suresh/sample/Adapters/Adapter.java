package com.suresh.sample.Adapters;



import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.suresh.sample.Activity.UpdateActivity;
import com.suresh.sample.Constants;
import com.suresh.sample.Model.Upload;
import com.suresh.sample.R;

import java.util.List;

import static android.provider.MediaStore.Audio.AudioColumns.ARTIST_ID;

/**
 * Created by Belal on 2/23/2017.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private Context context;
    private List<Upload> uploads;

    public Adapter(Context context, List<Upload> uploads) {
        this.uploads = uploads;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_admin_images, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Upload upload = uploads.get(position);
        holder.txtName.setText(upload.getName());
        holder.txtEmail.setText(upload.getEmail());
        holder.txtMobile.setText(upload.getMobile());
        holder.txtDate.setText("Posted on : "+upload.getCurrentDate());


        if (upload.type.equalsIgnoreCase("update")){
            try {
                Glide.with(context)
                        .load(upload.getImageurl())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                                holder.progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // image ready, hide progress now
                                holder.progressBar.setVisibility(View.GONE);
                                return false;   // return false if you want Glide to handle everything else.
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                        .into(holder.imageView);

            } catch (IndexOutOfBoundsException e) {
                // TODO: handle exception
                e.printStackTrace();

            }
        }else {
            try {
                Glide.with(context)
                        .load(upload.getUrl())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                                holder.progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // image ready, hide progress now
                                holder.progressBar.setVisibility(View.GONE);
                                return false;   // return false if you want Glide to handle everything else.
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                        .into(holder.imageView);

            } catch (IndexOutOfBoundsException e) {
                // TODO: handle exception
                e.printStackTrace();

            }
        }


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteArtist(upload.getId());
                deleteImageFromStorage(upload.getKey(),upload.getUrl());
                uploads.remove(position);
                notifyDataSetChanged();

            }
        });

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating an intent
                Intent intent = new Intent(context, UpdateActivity.class);

                //putting artist name and id to intent
                intent.putExtra("NAME",upload.getName());
                intent.putExtra("MOBILE", upload.getMobile());
                intent.putExtra("EMAIL", upload.getEmail());
                intent.putExtra("ID", upload.getId());
                intent.putExtra("mKEY", upload.getKey());
                intent.putExtra("URL", upload.getImageurl());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //starting the activity with intent
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return uploads.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName,txtMobile,txtDate,txtEmail;
        public ImageView imageView;
        public Button btnEdit,btnDelete;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtMobile = (TextView) itemView.findViewById(R.id.txtMobile);
            txtDate = (TextView) itemView.findViewById(R.id.currentDate);
            txtEmail = (TextView) itemView.findViewById(R.id.txtEmail);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            btnEdit = (Button) itemView.findViewById(R.id.btnEdit);
            btnDelete = (Button) itemView.findViewById(R.id.btnDelete);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);

        }
    }

    private boolean deleteArtist(String id) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS).child(id);

        //removing artist
        dR.removeValue();

        /*//getting the tracks reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(id);

        //removing all tracks
        drTracks.removeValue();*/

        Toast.makeText(context, "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private boolean deleteImageFromStorage(final String key, String imageUrl){

        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        final DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);

        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mDatabaseRef.child(key).removeValue();
                Toast.makeText(context, "Item deleted", Toast.LENGTH_SHORT).show();
            }
        });
        return true;
    }
}
