package com.suresh.sample.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.suresh.sample.Adapters.Adapter;
import com.suresh.sample.Constants;
import com.suresh.sample.Model.Upload;
import com.suresh.sample.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mainRecycler)
    RecyclerView mainRecycler;
    @BindView(R.id.addPost)
    FloatingActionButton addPost;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;


    //adapter object
    private Adapter adapter;

    //database reference
    private DatabaseReference mDatabase;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<Upload> uploads;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("USER DATA");

        prepareHomeData();


    }

    private void prepareHomeData() {

        simpleSwipeRefreshLayout.setRefreshing(true);

        uploads = new ArrayList<>();

        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                simpleSwipeRefreshLayout.setRefreshing(false);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {
                    txtError.setVisibility(View.GONE);
                    //iterating through all the values in database
                    uploads.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Upload upload = postSnapshot.getValue(Upload.class);
                        upload.setKey(postSnapshot.getKey());
                        uploads.add(upload);

                    }
                    //creating adapter
                    adapter = new Adapter(getApplicationContext(), uploads);
                    mainRecycler.setHasFixedSize(true);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, true);

                    // mainRecycler.setLayoutManager(new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, true));
                    //adding adapter to recyclerview
                    layoutManager.setReverseLayout(true);
                    layoutManager.setStackFromEnd(true);
                    mainRecycler.setLayoutManager(layoutManager);
                    mainRecycler.setAdapter(adapter);


                    adapter.notifyDataSetChanged();


                } else {

                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Posts Added Today");
                    mainRecycler.setAdapter(null);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                simpleSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }



    @OnClick(R.id.addPost)
    public void onViewClicked() {
        Intent intent = new Intent(getApplicationContext(), UploadActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



}
