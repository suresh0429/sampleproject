package com.suresh.sample.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.suresh.sample.Constants;
import com.suresh.sample.Model.Upload;
import com.suresh.sample.R;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateActivity extends AppCompatActivity {
    //constant to track image chooser intent
    private static final int PICK_IMAGE_REQUEST = 234;
    //this is the pic pdf code used in file chooser
    final static int PICK_PDF_CODE = 2342;
    private static final String TAG ="UpdateActivity" ;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.buttonUpload)
    Button buttonUpload;
    @BindView(R.id.UploadCv)
    Button UploadCv;
    @BindView(R.id.txtPdf)
    TextView txtPdf;


    //uri to store file
    private Uri filePath, pdfuri,filePath1;

    //firebase objects
    private StorageReference storageReference;
    private DatabaseReference mDatabase;
    private String formattedDate;
    StorageReference sRef;

    String id,mkey,url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("UPDATE DATA");


        if (getIntent() != null){

            String name = getIntent().getStringExtra("NAME");
            String mobile = getIntent().getStringExtra("MOBILE");
            String email = getIntent().getStringExtra("EMAIL");
             id = getIntent().getStringExtra("ID");
             mkey = getIntent().getStringExtra("mKEY");
             url = getIntent().getStringExtra("URL");

            etName.setText(name);
            etMobile.setText(mobile);
            etEmail.setText(email);

            Glide.with(UpdateActivity.this).load(url).into(imageView);
        }

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        /*@SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = df.format(c.getTime());

        storageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);


    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            Log.d(TAG, "onCreate: "+filePath);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == PICK_PDF_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            //if a file is selected
            if (data.getData() != null) {
                //uploading the file
                pdfuri = data.getData();
                txtPdf.setText(""+pdfuri.getPath());
            }else{
                Toast.makeText(this, "No file chosen", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @OnClick({R.id.imageView, R.id.buttonUpload, R.id.UploadCv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageView:
                showFileChooser();
                break;
            case R.id.UploadCv:
                getPDF();
                break;
            case R.id.buttonUpload:
                String name = etName.getText().toString();
                String email = etEmail.getText().toString();
                String mobile = etMobile.getText().toString();

                if (name.isEmpty()) {
                    etName.setError("Enter Name");
                    return;

                }
                if (email.isEmpty()) {
                    etEmail.setError("Enter Email");
                    return;

                }
                if (mobile.isEmpty()) {
                    etMobile.setError("Enter Mobile no");
                    return;

                }
                uploadFile(name, email, mobile);
                break;
        }
    }


    //this function will get the pdf from the storage
    private void getPDF() {
        //for greater than lolipop versions we need the permissions asked on runtime
        //so if the permission is not available user will go to the screen to allow storage permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }

        //creating an intent for file chooser
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_PDF_CODE);
    }


    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void uploadFile(final String name, final String email, final String mobile) {
        //checking if file is available

        //displaying progress dialog while image is uploading
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading");
        progressDialog.setCancelable(false);
        progressDialog.show();


        if (filePath != null) {
            //getting the storage reference
            sRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + System.currentTimeMillis() + "." + getFileExtension(filePath));

        } else {
            filePath = Uri.parse("android.resource://com.suresh.sample/drawable/" + R.drawable.rsr_logo);
            Log.e("URI", "" + filePath);
            sRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + "rsr_logo." + getFileExtension(filePath));

        }



        //adding the file to reference
        sRef.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //dismissing the progress dialog
                        progressDialog.dismiss();

                        Uri downloadUrl = taskSnapshot.getUploadSessionUri();
                        //displaying success toast

                        //adding an upload to firebase database
                       // String uploadId = mDatabase.push().getKey();


                        //creating the upload object to store uploaded image details
                        Upload upload = new Upload(id, formattedDate, name, email, mobile, taskSnapshot.getDownloadUrl().toString(),url,"update");
                        Log.e("IMAGEURL2", "" + taskSnapshot.getDownloadUrl().toString());

                        mDatabase.child(id).setValue(upload);

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d(TAG, "onFailure: "+exception.getMessage());
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        //displaying the upload progress
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                    }
                });

    }

    private void uploadPdf(){


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
