package com.suresh.sample.Interface;

public interface OnRecyclerViewItemClickListener {

    public void onRecyclerViewItemClicked(int position, int id);

}
